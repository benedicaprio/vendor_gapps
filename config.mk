ifeq ($(TARGET_G_ARCH),)
$(error "GAPPS: TARGET_G_ARCH is undefined")
endif

ifneq ($(TARGET_G_ARCH),arm64)
$(error "GOOGLE: Only arm64 is allowed")
endif

# app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    Chrome \
    Drive \
    GoogleCalendarSyncAdapter \
    GooglePrintRecommendationService \
    Maps \
    MarkupGoogle \
    Photos \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail

# priv-app
PRODUCT_PACKAGES += \
    GoogleContacts \
    GoogleDialer \
    StorageManagerGoogle \
    TagGoogle \
    TimeZoneDataPrebuilt \
    Velvet


